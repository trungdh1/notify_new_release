#!/bin/bash

ChatID="-769100612"
BotID="2005503415:AAFpSmLigeHNI_K5Xi7Iewa-eQ51mYywf0Y"

Array=('tron' 'ethereum' 'bsc' 'solana')

pattern="*{NAME} - NEW RELEASE*\n========<>+<>========\n\n*{VERSION}*\n{HTML}"

#urlencode function
urlencode_many_printf () {
  string=$1
  while [ -n "$string" ]; do
    tail=${string#?}
    head=${string%$tail}
    case $head in
      [-._~0-9A-Za-z]) printf %c "$head";;
      *) printf %%%02x "'$head"
    esac
    string=$tail
  done
  echo
}

tron=$(curl -H "Accept: application/vnd.github.v3+json" -H "Authorization: token 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" https://api.github.com/repos/tronprotocol/java-tron/releases/latest)
ethereum=$(curl -H "Accept: application/vnd.github.v3+json" -H "Authorization: token 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" https://api.github.com/repos/ethereum/go-ethereum/releases/latest)
bsc=$(curl -H "Accept: application/vnd.github.v3+json" -H "Authorization: token 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" https://api.github.com/repos/binance-chain/bsc/releases/latest)
solana=$(curl -H "Accept: application/vnd.github.v3+json" -H "Authorization: token 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" https://api.github.com/repos/solana-labs/solana/releases/latest)

# Sort Array
Array=($(for i in ${Array[@]}; do echo $i; done | sort)) 

# store data in tronV, ethereumV, ...
for i in ${Array[@]}; do
  declare "${i}V"=$i":$(echo ${!i} | sed -e 's/,/\n/g' | grep -m1 "name" | sed -e 's/"//g;s/name: //g' | sha256sum | awk '{print substr($0,1,32)}')"
done

#get description
GetChat="https://api.telegram.org/bot"$BotID"/getChat?chat_id="$ChatID
description=$(curl "$GetChat" | sed -e 's/,/\n/g;s/"//g' | grep description | sed -e 's/:/ /1' | awk '{print $2}' | sed -e 's/;/\n/g' )
description=($description)    #Convert String to Array

count=0
for i in ${Array[@]}; do
  temp=$i"V"
  echo =================================
  echo "${!temp}"
  echo =================================
  
  # not exist -> update
  if [[ ! "${description[*]}" =~ "${!temp}" ]]; then
    # add
    description[$((count))]=${!temp}
    URL=$(echo ${!i} | sed -e 's/,/\n/g' | grep -m1 html_url | sed -e 's/"//g;s/html_url: //g' ) 
    NAME=$(echo ${!i} | sed -e 's/,/\n/g' | grep -m1 '"name"' | sed -e 's/"//g;s/name: //g')
    ID=$(echo $i | tr "[a-z]" "[A-Z]")
    BODY=$(echo $pattern | awk -v ID="$ID" -v URL="$URL" -v NAME="$NAME" '{sub(/{NAME}/, ID); sub(/{VERSION}/, NAME); sub(/{HTML}/, URL); print}' ) 
    BODY=$(urlencode_many_printf "$BODY" | sed -e 's/%5c%6e/%0a/g;s/%5cn/%0a/g')

    #notify
    $(curl "https://api.telegram.org/bot$BotID/sendMessage?chat_id=$ChatID&parse_mode=Markdown&text=$BODY" &>/dev/null)
  fi
  count=$((count+1))
done

#UpdateDescription
updateDes=$(echo ${description[@]} | sed -e 's/ /;/g' ) 
echo "${updateDes}"
$(curl "https://api.telegram.org/bot$BotID/setChatDescription?chat_id=$ChatID" --data-urlencode "description=$updateDes" &>/dev/null)
